import { Component } from "react";
import { Message } from './Message';
import { OwnMessage } from './OwnMessage';
import './styles/messageList.css';

export class MessageList extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.data === nextProps.data) {
            return false;
        } else {
            return true;
        }
    }
    
    render() {
        const DividerFormatter = new Intl.DateTimeFormat("en-AU", {
            weekday: "long",
            day: "numeric",
            month: "long"
        });

        const messageList = this.props.data?.map(message => {
            return message.userId === this.props.ownId ?
            <OwnMessage
                text={message.text}
                messageTime={message.createdAt}
                key={message.id}
                id={message.id}
                onDelete={this.props.onDataChange}
                onEdit={this.props.onMessageEdit}
            >
            </OwnMessage>
            :
            <Message
                text={message.text}
                messageTime={message.createdAt}
                like="like"
                username={message.user}
                userAvatar={message.avatar}
                key={message.id}
            >
            </Message>
        })

        const messageListDivided = function() {
            let prevMessageDate;
            let messageListDivided = [];
            for (let i = 0; i < messageList.length; i++){
                const datePrev = new Date(prevMessageDate);
                const dateNow = new Date(messageList[i].props.messageTime);
                let newDate = false;
                if (prevMessageDate) {
                    if (datePrev.getFullYear() === dateNow.getFullYear() &&
                        datePrev.getMonth() === dateNow.getMonth() &&
                        datePrev.getDate() === dateNow.getDate()) {            
                    } else {
                        newDate = true;
                    }
                } else {
                    const today = new Date();
                    if (today.getFullYear() === dateNow.getFullYear() &&
                        today.getMonth() === dateNow.getMonth() &&
                        today.getDate() === dateNow.getDate()) {
                            messageListDivided.push(<div className="messages-divider" key={dateNow}>Today</div>)
                    } else {
                        const yesterday = new Date(today);
                        yesterday.setDate(yesterday.getDate() - 1)
                        if (yesterday.getFullYear() === dateNow.getFullYear() &&
                            yesterday.getMonth() === dateNow.getMonth() &&
                            yesterday.getDate() === dateNow.getDate()) {
                                messageListDivided.push(<div className="messages-divider" key={dateNow}>Yesterday</div>)
                    } else {
                        messageListDivided.push(<div className="messages-divider" key={dateNow}>{DividerFormatter.format(dateNow)}</div>)
                    }
                    }
                }
                prevMessageDate = messageList[i].props.messageTime;
                if (newDate) {
                    const today = new Date();
                    if (today.getFullYear() === dateNow.getFullYear() &&
                        today.getMonth() === dateNow.getMonth() &&
                        today.getDate() === dateNow.getDate()) {
                            messageListDivided.push(<div className="messages-divider" key={dateNow}>Today</div>)
                    } else {
                        const yesterday = new Date(today);
                        yesterday.setDate(yesterday.getDate() - 1)
                        if (yesterday.getFullYear() === dateNow.getFullYear() &&
                            yesterday.getMonth() === dateNow.getMonth() &&
                            yesterday.getDate() === dateNow.getDate()) {
                                messageListDivided.push(<div className="messages-divider" key={dateNow}>Yesterday</div>)
                        } else {
                            messageListDivided.push(<div className="messages-divider" key={dateNow}>{DividerFormatter.format(dateNow)}</div>)
                        }
                    }
                    messageListDivided.push(messageList[i]);
                } else {
                    messageListDivided.push(messageList[i]);
                }
            }
            return messageListDivided;
        }

        return (
            <div className="message-list">
                {this.props.data ? messageListDivided() : null}
            </div>
        );
    }
}