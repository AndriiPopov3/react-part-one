import './styles/messageInput.css';

export function MessageInput(props) {
    return (
        <div className="message-input">
            <input className="message-input-text" onChange={props.onInputChange} value={props.value}></input>
            <button className="message-input-button" onClick={props.sendMessage}>{props.type}</button>
        </div>
    );
}