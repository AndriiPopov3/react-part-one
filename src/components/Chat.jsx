import { v4 as uuidv4 } from 'uuid';
import { Component } from "react";
import { Preloader } from './Preloader';
import { Header } from './Header';
import { MessageList } from './MessageList';
import { MessageInput } from './MessageInput';

export class Chat extends Component {
    constructor(props) {
        super(props);
        this.onDataChange = this.onDataChange.bind(this);
        this.onMessageEdit = this.onMessageEdit.bind(this);
        this.state = {
            data: null,
            newMessage: "",
            isLoaded: false,
            isEdited: false,
            editedMessageId: ""
          };
          this.ownUserId = "7470ff13-ade9-457a-a9bb-a8c35e87d3fe";
    } 

    LastMessageFormatter = new Intl.DateTimeFormat("ru", {
        year: "numeric",
        month: "numeric",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit"
    });

    componentDidMount() {
        fetch(this.props.url)
          .then(res => res.json())
          .then(data => {
            this.setState({data});
            this.setState({isLoaded: true});
          });
    }

    countUsers() {
        let res = 1;
        for (let i = 1; i < this.state.data.length; i++) {
            let j = 0;
            for (j = 0; j < i; j++)
                if (this.state.data[i].user === this.state.data[j].user)
                    break;
            if (i === j)
                res++;
        }
        return res;
    }

    sendMessage() {
        if (this.state.newMessage !== "") {
            const timeNow = new Date();
            const newMessage = {
                id: uuidv4(),
                userId: this.ownUserId,
                avatar: "https://t2.genius.com/unsafe/440x440/https%3A%2F%2Fimages.genius.com%2F09acba93727a483f8ed0579ba60dce61.715x715x1.jpg",
                user: "Templar",
                text: this.state.newMessage,
                createdAt: timeNow,
                editedAt: ""
            }
            if (this.state.data) {
                const messages = this.state.data.concat(newMessage);
                this.setState({data: messages});
            } else {
                const messages = [];
                messages.push(newMessage);
                this.setState({data: messages});
            }
            this.setState({newMessage: ""});
        }
    }

    onInputChange(event) {
        this.setState({newMessage: event.target.value})
    }

    onDataChange(id) {
        for (let i = 0; i < this.state.data.length; i++) {
            const array = [...this.state.data]
            if (this.state.data[i].id === id) {
                array.splice(i, 1);
                this.setState({data: array});
                this.setState({isEdited: false});
            }
        }
    }

    onMessageEdit(id) {
        for (let i = 0; i < this.state.data.length; i++) {
            if (this.state.data[i].id === id) {
                this.setState({newMessage: this.state.data[i].text});
                this.setState({isEdited: true});
                this.setState({editedMessageId: id});
            }
        }
    }

    editMessage() {
        const messages = [...this.state.data];
        for (let i = 0; i < this.state.data.length; i++) {
            if (messages[i].id === this.state.editedMessageId) {
                messages[i].text = this.state.newMessage;
                messages[i].editedAt = new Date();
                this.setState({data: messages});
                this.setState({newMessage: ""});
                this.setState({isEdited: false});
                this.setState({editedMessageId: ""});
            }
        }
    }

    render() {
        const { data } = this.state;
        return (
            <>
                {!this.state.isLoaded ?
                    <Preloader />
                :
                    <> 
                        <Header 
                        chatTitle="My chat" 
                        userCount={data ? (data.length > 0 ? this.countUsers() : 0) : 0}
                        messageCount={data ? data.length : 0}
                        lastMessageDate={data ? (data.length > 0 ? this.LastMessageFormatter.format(new Date(data[data.length - 1].createdAt)).replace(/,/g, '') : "none") : "none"}
                        >
                        </Header>
                        <MessageList
                            data={data}
                            onDataChange={this.onDataChange}
                            onMessageEdit={this.onMessageEdit}
                            ownId={this.ownUserId}
                        >
                        </MessageList>
                        <MessageInput
                            value={this.state.newMessage}
                            onInputChange={(event) => this.onInputChange(event)}
                            sendMessage={this.state.isEdited ? (event) => this.editMessage(event) : (event) => this.sendMessage(event)}
                            type={this.state.isEdited ? "Edit" : "Send"}
                        >
                        </MessageInput>
                    </>
                }
            </>
        );
    }
}