import logo from '../logo.svg';
import './styles/preloader.css';

export function Preloader(props) {
    return (
        <div className="preloader">
            <img src={logo} className="preloader-logo" alt="logo" />
        </div>
    );
}