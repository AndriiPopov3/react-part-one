import { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons'
import './styles/message.css';

export class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLiked: false
        }
    }

    MessageFormatter = new Intl.DateTimeFormat("ru", {
        hour: "2-digit",
        minute: "2-digit"
    });

    likeComment() {
        this.setState({isLiked: !this.state.isLiked});
    }

    render() {
        return (
            <div className="message">
                <div className="user-info-container">
                    <img className="message-user-avatar" src={this.props.userAvatar} alt="user-avatar"></img>
                    <div className="message-user-name">{this.props.username}</div>
                </div>
                <div className="message-text-container">
                    <div className="message-text">{this.props.text}</div>
                    <div className="message-info-container">
                        <div className="message-time">{this.MessageFormatter.format(new Date(this.props.messageTime))}</div>
                            <FontAwesomeIcon
                                icon={faThumbsUp}
                                className={this.state.isLiked ? "message-liked" : "message-like"}
                                onClick={() => this.likeComment()}
                            />
                    </div>
                </div>
            </div>
        );
    }
}